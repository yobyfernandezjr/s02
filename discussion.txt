Common CLI commands
mkdir - create a new folder/directory
	syntax:
		mkdir <folderName>
		eg. mkdir batch-197
touch - used to create files
	syntax:
		touch <fileName>
		eg. touch discussion.txt
cd - change directory
	- change the current folder/directory we are currently working on with our CLI (terminal/gitbash)
	syntax:
		cd <folderName/path to folder>
		eg. cd Documents
cd .. - it moves back one folder/directory
	syntax:
		cd ..
ls - list
	- it list files and folders contained by the current directory
	syntax:
		ls
pwd - present working directory
	- shows the current directory we are working on.
	syntax:
		pwd
When using git for the first time in your computer/machine
Configure our Git: 
	- git config --global user.email "<emailFromGitlab>""
	- this will allow us to identify the account from Gitlab/Github who will push/upload files into our online Gitlab or Github services.

	- git config --global user.name "<nameUsedInGitLab>"
		- this will allow us to identify the name of the user who is trying to upload/push files into our online GitLab or GitHub services.
			- this allows us to determine the name of the person who uploaded the latest commit/version in our repo.

What is SSH key?
	- SSH or Secure SHell key
		- tools used to authenticate the uploading or of other tasks when manipulating or using git repositories.
		- it allows us to push/upload into our online git repo without the use of passwords.

	ssh-keygen -t ed25519
		- t option to select algorithm
		- ed25519 is the new algorithm added in OpenSS

Git commands - pushing/uploading:

======
Name: Yoby
Address: Cainta, Rizal

copy git remote add origin ...
type git remote -v
type git push origin master
check gitlab